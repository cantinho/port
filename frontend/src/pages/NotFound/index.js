import React from 'react';
import { Link } from 'react-router-dom';

const Page = () => {
    return (
        <div>
            Página Não Encontrada.<br/>

            <Link to="/">Voltar para a HOME</Link>
        </div>
    );
}

export default Page;